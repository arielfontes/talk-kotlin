
val sql = """
    select
        name,
        age
    from user
    where
        name = :name
        and age < 30
    order by age desc
    """

fun main(args : Array<String>) {
    println(sql)

    val param = "world"

    println("hello $param")

    println("param has ${param.length} characters")

    println("1 + 2 = ${1+2}")

    println("\$dolares ")
}
