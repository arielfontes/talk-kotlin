
class AlgumaCoisaFactory private constructor(){

    companion object {
        @JvmStatic
        val JVM_STATICO = "static jvm"

        val VALOR_OBJECT = "valor"

        fun create() : AlgumaCoisaFactory {
            return AlgumaCoisaFactory()
        }
    }
}


fun main(args: Array<String>) {

    val x = AlgumaCoisaFactory.create()

    print(AlgumaCoisaFactory.VALOR_OBJECT)

    //java
    // System.out.println(AlgumaCoisaFactory.Companion.getVALOR_OBJECT());
    // System.out.println(AlgumaCoisaFactory.getJVM_STATICO());

}

