import org.junit.Test

// if expression

// nao existe operador ternario em kotlin
val result = if (1==1) "verdadeiro" else "false"


val result2 = if (1 < 2) {
        println("condi. verdadeira")
        true
    } else {
        print("fasdf")
        false
    }


// ==== when
class T1 {
    @Test fun xx() {
        val x = 1
        when (x) {
            1, 2 -> println("x == 1 or x ==2")
            in 3..6 -> println("x in range 3..6 ")
            !in 10..20 -> println("x is outside the range")
            is Int -> println("x is Int") //(is or !is)
            else -> { // Note the block
                println("x is neither")
            }
        }

        val y = "123"
        // pode substituir if-else if chain

        when {
            y.length > 2 -> println("x is > 2")
            y.length == 5 -> println("x equals 5")
            else -> print("x is funny")
        }
    }
}

// ==== for loop

class T2 {
   @Test fun xxx() {
       val collection = arrayOf(4,5,6)

        for (item in collection) {
            println(item)
        }

       // iteration over range expressions
       for (i in 1..3) println(i)

       for (i in 6 downTo 0 step 2) println(i)
    }
}

data class Item(var id: Int, var name: String, var age: Int)

fun ArrayList<Item>.findBenhur(): Item?{
    return this.find { it.name == "Benhur"}
}


fun main(args: Array<String>) {

    var list = ArrayList<Item>()
    list.add(Item(1, "name 1",10))
    list.add(Item(2, "name 2",25))
    list.add(Item(3, "name 3",5))
    list.add(Item(4, "name 4",72))
    list.add(Item(5, "name 5",83))
    list.add(Item(6, "name 6",12))
    list.add(Item(7, "name 7",9))
    list.add(Item(8, "name 08",5))
    list.add(Item(9, "Benhur",83))

    val item = list.find { it.age == 12 }
    println(item?.name)
    println("=======================================")

    list.sortBy { it.name }
    list.forEach{
        println(it.name)
    }

    println("=======================================")

//    list.sortWith(compareBy({ it.age }, { it.name }))
    list.sortWith(compareBy( Item::age ,  Item::name ))
    list.forEach{
        println("nome: ${it.name}, idade: ${it.age}")
    }

    println("=======================================")
    println(list.findBenhur().toString())

}


//fun main(args: Array<String>){
//    var list : MutableList<Int> = mutableListOf(1,2,3,4,5,6,7,8,9,10,20,30)
//    list.forEach{
//        println(it)
//    }
//
//    list.forEachIndexed { index, i -> println("$index -> $i")  }
//
//    val item20 = list.find { it%20 == 0 }
//    println(item20)
//
//    //Retorna o primeiro elemento
//    val item10= list.find{ it%10 == 0}
//    println(item10)
//
//    //Retorna o ultimo elemento
//    val itemLast10 = list.findLast { it%10 == 0 }
//    println(itemLast10)
//
//    //Retorna uma lista
//    val itemList10 = list.filter { it%5 == 0 }
//    itemList10.forEach {
//        println(it)
//    }
//
//}

// while loop igual java