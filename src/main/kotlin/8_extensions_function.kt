fun String.removeFirstLastChar(): String =  this.substring(1, this.length - 1)
fun Int.negative(): Int = this*(-1)
fun Int.par(): Boolean = this%2==0
fun Int.impar() = !this.par()

fun main(args: Array<String>) {
    println(10.impar())

    println("kotlin".removeFirstLastChar())
}
