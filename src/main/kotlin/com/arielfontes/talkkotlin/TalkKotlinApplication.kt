package com.arielfontes.talkkotlin

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class TalkKotlinApplication

fun main(args: Array<String>) {
    runApplication<TalkKotlinApplication>(*args)
}
