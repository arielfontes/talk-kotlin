

fun main(args: Array<String>) {
    val a: Int = 1  // immediate assignment
    val b = 2   // `Int` type is inferred
    val c: Int  // Type required when no initializer is provided

}
// constants
fun runtimeFunction() = 1+2

val CONST_FIELD = "valor"
val RUNTIME_CONST = runtimeFunction()

// compile time constant
const val MAX_COUNT = 8
