import org.junit.Test

class ClasseTeste {

    fun sum(x: Int, y: Int): Int {
        return x + y
    }

    //functions with expression body
    fun multply(x: Int, y: Int) = x * y

    // default param
    fun read(b: Array<Byte>, startAt: Int = 0, length: Int = b.size) {
        // (...)
    }

    var text = read(arrayOf())
    var text2 = read(arrayOf(), 12)
    var text3 = read(b = arrayOf(), startAt = 432, length = 3000)

    @Test
    fun `deveria funcionar`() {
        assert (1 == 2)
    }
}


// method param

fun metodo1(param1: String = "vazio", paramMetodo: () -> Unit){
    println("metodo1")
    paramMetodo()
}

fun metodo2(){
    println("metodo2")
}

//Passando um valor para o metodo
fun metodo3(param1: String = "vazio", paramMetodo: (String) -> Unit){
    println("metodo3")
    paramMetodo("passando um novo parametro")
}

fun metodo4(param1: String){
    println("metodo4 -> $param1")
}

//Metodo dentro de metodo
fun metodo5(num1: Int, num2: Int): Int {

    fun inceptionSum(num: Int): Int{
        return num * 10
    }

    return inceptionSum(num1) + inceptionSum(num2)
}

//Metodos genéricos
fun <GENERICO> metodo6(item: GENERICO): GENERICO {
    return item
}



fun main (args: Array<String>) {
    metodo1 { metodo2() }
    metodo3 {  param -> metodo4("outra coisa") }

    println(metodo5(1,2))

    println(metodo6("Metodo genérico"))
}

