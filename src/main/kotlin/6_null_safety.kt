// basic type and checking
fun main(args: Array<String>) {
    var y : String = "oi"
//    y = null // nao é permitido

    y.length


    var x : String?
    x = null


    // safe cals ( ?. )
    println(x?.length)


    // elvis operator
    val l = x?.length ?: "novo valor"

    val l2 = x?.length ?: throw IllegalArgumentException()

    // throw nullpointer
    x!!.length



}








