import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import javax.persistence.*

// jpa

@Entity
class User(

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    var id: Long,

    @Column(name="NOME")
    var name: String,

    @OneToMany(cascade = [CascadeType.ALL])
    var pets: Array<String>) {

    // body (...)
}

@Service
class Servico @Autowired constructor(
        foo: String,
        bar: Int) {
    // (...)
}

@Service
class Service {

    val log : Logger = LoggerFactory.getLogger(Service::class.java)

    @Autowired
    lateinit var repo1 : Servico

    @Value("\${value.from.file}")
    lateinit var value: String // only var

    // verifica se repo1 foi inicializado
    fun foo() = this::repo1.isInitialized
}


