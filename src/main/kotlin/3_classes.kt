import org.springframework.beans.factory.annotation.Autowired


class Usuario (var nome: String, private var idade: Int)

// implementa( toString, equals, hashCode )
data class Endereco(var logradouro: String, var numero: Int)

class Myclass {
    var validField: Int
    set(value){
        if (value != 0) {
            validField = value
        }
    }
    get() = validField + 1

    val x1: String

    init {
        x1 = "primeiro"
    }
}

fun main (args: Array<String>) {
    val end = Endereco("alvoro rodrigues", 352)
    val usuario = Usuario("josé", 20)

    //to string
    println(end)
    println(usuario)

    // acesso field
    println(usuario.nome)

}


// interface
interface Inter1 {
    fun method()
}

open class Base(value: String) : Inter1 {
    override fun method() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}

class Derived(value: String): Base(value)


